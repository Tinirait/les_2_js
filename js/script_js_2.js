var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];
console.log('Список студентов:');
console.log('N=1.Вывести список в следующем виде');
for (var i = 1; i < studentsAndPoints.length; i+=2) {
	console.log('Студент ' +studentsAndPoints[i-1]+ ' набрал ' + studentsAndPoints[i]+' баллов ');
}

console.log('N=2.Cтудент набравший максимальный балл')
var maxIndex = -1, max; 
for (var i = 1, imax = studentsAndPoints.length; i < imax; i+=2) {
	if (maxIndex < 0 || studentsAndPoints[i] > max) {
		maxIndex = i;
		max = studentsAndPoints[i]; 
	}
}
console.log('Студент', studentsAndPoints[maxIndex-1],'имеет максимальный балл',studentsAndPoints[maxIndex]);

console.log('N=3 Добавили новых студентов');
studentsAndPoints.push('Николай Фролов',0, 'Олег Боровой', 0);
for(var i = 1; i < studentsAndPoints.length; i+=2){
	if(i % 2 == 0){
		var student = studentsAndPoints[i];
	}
	console.log('Студент ' +studentsAndPoints[i-1]+ ' набрал ' + studentsAndPoints[i] + ' баллов');
}

console.log('N=4 Студенты «Антон Павлович» и «Николай Фролов» и «Дмитрий Фитискин» набрали еще по 10 баллов, внес изменения в массив');
var newbalforse = ['Антон Павлович', 'Николай Фролов', 'Дмитрий Фитискин'];
  for (var i = 0; i < newbalforse.length; i++) {
    if (-1 != studentsAndPoints.indexOf(newbalforse[i])) {
      studentsAndPoints[studentsAndPoints.indexOf(newbalforse[i])+1]+=10;
      console.log(studentsAndPoints[studentsAndPoints.indexOf(newbalforse[i])] + ' найден добавляем существующему студенту +10 баллов');
    } else {
      console.log('Студент не найден ' + newbalforse[i] + ' Такого студента не существует')
    }
  } 
  /*Предыдущий доработанный вариант cоздание отдельной переменной под студента Дмитрия Фитискина,если такого 
console.log('N=4 Студенты «Антон Павлович» и «Николай Фролов» и «Дмитрий Фитискин» набрали еще по 10 баллов, внес изменения в массив');
studentsAndPoints[studentsAndPoints.indexOf("Антон Павлович")+1] +=10;
studentsAndPoints[studentsAndPoints.indexOf("Николай Фролов")+1] +=10;
var dfitiskin=studentsAndPoints.indexOf("Дмитрий Фитискин");
if (dfitiskin<0){
  studentsAndPoints.push("Дмитрий Фитискин",10);
}else{
  studentsAndPoints[dfitiskin+1]+=10;
}
for(var i = 1; i < studentsAndPoints.length; i+=2){
	if(i % 2 !== 0){
		var student = studentsAndPoints[i-1];
		var bal = studentsAndPoints[i];
	}
	console.log('Студент ' + student + ' набрал ' + bal + ' баллов');		
}
*/
console.log('N=5.Студенты, не набравшие ни одного балла:');
var studentsAndPointsWithoutLoosers = [];
for(var i = 1; i < studentsAndPoints.length; i+=2){
	var bal = studentsAndPoints[i];
	if(bal == 0){
		console.log('Студент ' + studentsAndPoints[i-1] + ' набрал ' + bal + ' баллов');
	}
}

console.log('N=6. Исключаем студентов, набравших 0 баллов.');
while(studentsAndPoints.indexOf(0)>-1){
	var i = studentsAndPoints.indexOf(0);
	studentsAndPoints.splice(i-1, 2);
}
console.log("Итоговый список студентов:");
for (var i = 1; i < studentsAndPoints.length; i+=2) {
	console.log('Студент ' +studentsAndPoints[i-1]+ ' набрал ' + studentsAndPoints[i]+' баллов ');
}